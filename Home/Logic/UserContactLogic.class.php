<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/12
 * Time: 17:19
 */
namespace Home\Logic;

use Think\Model\RelationModel;

class UserContactLogic extends RelationModel {
    /*
    * 查询关联好友
    */
    public function addfriend($user_id) {
        if ($user_id) {
            $user_contactList = $this->getContact($user_id);
            return toJson(1, $user_contactList);
        } else {
            return toJson(-1, "The parameter is incorrect!");
        }
    }

    /*
    * 添加好友信息
    */
    public function addfriends() {


    }

    /*
     * 解除绑定
     */
    public function Rebingding($user_id, $binding_id) {
        $where = "binding_id='{$binding_id}' and user_id='{$user_id}'";
        $whe = "binding_id='{$user_id}' and user_id='{$binding_id}'";
        $user_id = M('user_contact')->where($where)->find();
        if ($user_id) {
            $delete_id = M('user_contact')->where($where)->delete();
            $detwo_id = M('user_contact')->where($whe)->delete();
            if ($delete_id && $detwo_id) {
                return toJson(1, "successfully deleted！");
            } else {
                return toJson(1, "failed to delete！");
            }
        } else {
            return toJson(-1, "User does not exist！");
        }
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getContact($user_id) {
        $user_contactList = M('user_contact')->where("user_id=$user_id ")->select();
        foreach ($user_contactList as $k => $v) {
            $user_List = M('user')->where("user_id =" . $v['binding_id'])->find();
            $v['user_List'] = $user_List;
            $user_contactList[$k] = $v;
        }
        return $user_contactList;
    }

    public function getContact_ok($user_id) {
        $user_contactList = M('user_contact')->where("user_id=$user_id and state='ok'")->select();
        foreach ($user_contactList as $k => $v) {
            $user_List = M('user')->where("user_id =" . $v['binding_id'])->find();
            $v['user_List'] = $user_List;
            $user_contactList[$k] = $v;
        }
        return $user_contactList;
    }
}