<?php
namespace Home\Controller;

use Think\Controller;
use \JPush\Client as Jpush;

class IndexController extends ApiBaseController {
    public $userLogic;

    /**
     * 析构流函数
     */
    public function __construct() {
        parent::__construct();

    }

    public function _initialize() {
        parent::_initialize();
        $this->userLogic = new \Home\Logic\UserLogic();
    }

    /**
     * 登录
     */
    public function login() {
        $code = $_POST["code"];
        $user_phone = $_POST["user_phone"];
        $data = $this->userLogic->app_login($user_phone, $code);
        $this->ajaxReturn($data);
    }

    /**
     * 修改个人信息
     */
    public function changeUser() {
        $user_id = $_POST["user_id"];
        $first_name = $_POST["first_name"];
        $last_name = $_POST["last_name"];
        $sex = $_POST["sex"];
        $head_image = $_POST["head_image"];
        $help_text = $_POST["help_text"];
        $repeat_times = $_POST["repeat_times"];
        $data = $this->userLogic->change_User(array("first_name" => $first_name, "last_name" => $last_name, "sex" => $sex,
            "head_image" => $head_image, "help_text" => $help_text, "repeat_times" => $repeat_times, "user_id" => $user_id));
        $this->ajaxReturn($data);;
    }

    /**
     * 查询个人信息
     */
    public function queryUser() {
        $user_id = $_POST["user_id"];
        if (!$user_id) {
            $this->ajaxReturn(toJson(-1, "The parameter is empty"));
        }
        $user = M('user')->where("user_id='{$user_id}'")->find();
        if (!$user) {
            $this->ajaxReturn(toJson(-1, "The user is not registered"));
        }
        $user_contactList = M('user_contact')->where("user_id=$user_id and state = 'ok'")->select();
        $user["friend"] = count($user_contactList);
        $this->ajaxReturn(toJson(1, $user));
    }

    //发送绑定信息
    public function sendBinding() {
        $user_id = $_POST["user_id"];
        $user_phone = $_POST["user_phone"];
        $this->ajaxReturn($this->userLogic->send_Binding($user_phone, $user_id));;
    }

    //接受或者拒绝
    public function isBinding() {
        $user_id = $_POST["user_id"];
        $binding_id = $_POST["binding_id"];
        $state = $_POST["state"];
        $this->ajaxReturn($this->userLogic->is_Binding($user_id, $binding_id, $state));;
    }

    public function Findex() {
        header("Content-type: text/html; charset=utf-8");
        $maxSize = 1024 * 1024; //1M 设置附件上传大小
        $allowExts = array("gif", "jpg", "jpeg", "png"); // 设置附件上传类型
        $file_save = "Public/upload/";
        include_once("./UploadFile.class.php");
        $upload = new UploadFile(); // 实例化上传类
        $upload->maxSize = $maxSize;
        $upload->allowExts = $allowExts;
        $upload->savePath = $file_save; // 设置附件
        $upload->saveRule = time() . sprintf('%04s', mt_rand(0, 1000));
        if (!$upload->upload()) {// 上传错误提示错误信息
            $errormsg = $upload->getErrorMsg();
            $arr = array(
                'error' => $errormsg, //返回错误
            );
            echo json_encode($arr);
            exit;
        } else {// 上传成功 获取上传文件信息
            $info = $upload->getUploadFileInfo();
            $imgurl = "/" . $info[0]['savepath'] . $info[0]['savename'];
            $this->ajaxReturn(toJson(1, $imgurl));;
        }
    }

    /**
     * 发短信
     */
    public function sendCode() {
        $phone = $_POST["phone"];
        $this->ajaxReturn($this->userLogic->send_Code($phone));
    }

    /**
     * api
     */
    public function api() {
        $this->display();
    }

    public function userDeal() {
        $this->display();
    }

//    public function index() {
//        echo "有网";
//    }

    public function pushDemo() {
        $client = new Jpush();
        $data = $client->push()
            ->setPlatform('all')
            ->addAllAudience()
            ->setNotificationAlert('Hello, JPush')
            ->send();
        echo print_r($data);
    }
}