<?php

/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: 当燃
 * Date: 2015-09-09
 */

namespace Home\Controller;

use Think\Controller;

//use Admin\Logic\UpgradeLogic;
class ApiBaseController extends Controller {

    /**
     * 析构函数
     */
    function __construct() {
        parent::__construct();
//        $upgradeLogic = new UpgradeLogic();
//        $upgradeMsg = $upgradeLogic->checkVersion(); //升级包消息
//        $this->assign('upgradeMsg',$upgradeMsg);
        //用户中心面包屑导航
//        $navigate_admin = navigate_admin();
//        $this->assign('navigate_admin',$navigate_admin);
//        tpversion();
    }

    /*
     * 初始化操作
     */
    public function _initialize() {
    }

}