<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/12
 * Time: 16:46
 */

namespace Home\Controller;

use Think\Controller;

class RelieveController extends ApiBaseController
{

    /**
     * 析构流函数
     */
    public function __construct()
    {
        parent::__construct();

    }

    public $relievebing;

    public function _initialize()
    {
        parent::_initialize();
        $this->relievebing = new \Home\Logic\UserContactLogic();
    }


    /*
    *  删除好友
    */
    public function relieve()
    {
        $user_id = $_POST["user_id"];
        $binding_id = $_POST["binding_id"];

        $data = $this->relievebing->Rebingding($user_id,$binding_id);

        echo $this->ajaxReturn($data);
    }

    /*
    *   查询好友
    */

    public function find()
    {
        $user_id = $_GET["user_id"];
        $data = $this->relievebing->addfriend($user_id);
        echo $this->ajaxReturn($data);
    }

    /*
    *  添加好友
    */
    public function addfr()
    {

    }

}