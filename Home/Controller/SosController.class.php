<?php
namespace Home\Controller;

use Think\Controller;

class SosController extends ApiBaseController {
    public $UserSosLogic;

    /**
     * 析构流函数
     */
    public function __construct() {
        parent::__construct();

    }

    public function _initialize() {
        parent::_initialize();
        $this->UserSosLogic = new \Home\Logic\UserSosLogic();
    }

    //报警
    public function sos() {
        $user_id = $_POST["user_id"];
        $location = $_POST["location"];
        $add_res = $_POST["add_res"];
        $this->ajaxReturn($this->UserSosLogic->to_sos($user_id, $location, $add_res));
    }

    //接受报警
    public function AcceptSos() {
        $user_id = $_POST["user_id"];
        $push_id = $_POST["push_id"];
        $this->ajaxReturn($this->UserSosLogic->Accept_Sos($user_id, $push_id));
    }

    //查询报警信息
    public function querySos() {
        $sos_id = $_POST["sos_id"];
        $this->ajaxReturn(toJson(1, $this->UserSosLogic->getLocationBySosId($sos_id)));
    }

    //上传定位
    public function setLocation() {
        $sos_id = $_POST["sos_id"];
        $location = $_POST["location"];
        $this->ajaxReturn($this->UserSosLogic->set_Location($sos_id, $location));
    }

    //查询我报警的记录 返回推送所有的好友
    public function querySosByPush() {
        $user_id = $_POST["user_id"];
        $this->ajaxReturn($this->UserSosLogic->query_SosByPush($user_id));
    }

    //删除某个我报警的记录
    public function deleteSosByPush() {
        $sos_id = $_POST["sos_id"];
        $this->ajaxReturn($this->UserSosLogic->delete_SosByPush($sos_id));
    }

    //查询别人向我报警的记录
    public function queryUserByPush() {
        $user_id = $_POST["user_id"];
        $this->ajaxReturn($this->UserSosLogic->query_UserByPush($user_id));
    }

    //删除莫某个人向我报警的记录
    public function deleteUserByPush() {
        $user_id = $_POST["user_id"];
        $delete_user_id = $_POST["delete_user_id"];
        $this->ajaxReturn($this->UserSosLogic->delete_UserByPush($user_id, $delete_user_id));
    }

    //查询该用户推送信息
    public function queryUserPush() {
        $user_id = $_POST["user_id"];
        $this->ajaxReturn($this->UserSosLogic->query_UserPush($user_id));
    }

    // 查询该用户邀请他的好友信息
    public function queryFriendList() {
        $user_id = $_POST["user_id"];
        $this->ajaxReturn($this->UserSosLogic->query_FriendList($user_id));
    }
    //demo

}